Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: AUGUSTUS
Source: http://bioinf.uni-greifswald.de/augustus/
Files-Excluded: *.o
                bin
                auxprogs/filterBam/data
                docs/*.pdf
                docs/mysql.*
                docs/ncmodel.svg
                docs/exonlocalmalus.png
                docs/aliit.png
                docs/readme.rnaseq.html
                auxprogs/bam2hints/bam2hints
                auxprogs/bam2wig/bam2wig
                auxprogs/compileSpliceCands/compileSpliceCands
                auxprogs/filterBam/src/filterBam
                auxprogs/homGeneMapping/src/homGeneMapping
                auxprogs/joingenes/gp_call
                auxprogs/joingenes/joingenes
                scripts/aln2wig/aln2wig
                augustus-training


Files: *
Copyright: © 2004-2016 Lizzy Gerischer, Oliver Keller, Stefanie König,
                       Lars Romoth, Mario Stanke, Emmanouil Stafilarakis
License: Artistic

Files: scripts/webserver-results.tail
Copyright: © 2011 University of Greifswald
License: Artistic

Files: auxprogs/filterBam/src/bamtools/*.h
Copyright: © 2009-2010 Derek Barnett, Erik Garrison, Gabor Marth,
                       Michael Stromberg
License: MIT

Files: debian/*
Copyright: © 2016 Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
License: GPL-3+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

