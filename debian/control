Source: augustus
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Sascha Steinbiss <satta@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libsqlite3-dev,
               libboost-iostreams-dev,
               zlib1g-dev,
               libgsl-dev,
               liblpsolve55-dev,
               libbamtools-dev,
               libbz2-dev,
               lzma-dev,
               libhts-dev,
               libncurses5-dev,
               libboost-serialization-dev,
               libssl-dev,
               libmysql++-dev,
               asciidoctor,
               python3,
               samtools
Standards-Version: 4.5.1
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/med-team/augustus
Vcs-Git: https://salsa.debian.org/med-team/augustus.git
Homepage: https://bioinf.uni-greifswald.de/augustus/

Package: augustus
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         augustus-data
Recommends: samtools
# For the bundled scripts.
Suggests: cdbfasta,
          diamond-aligner,
          libfile-which-perl,
          libparallel-forkmanager-perl,
          libyaml-perl,
          libdbd-mysql-perl,
          python3-biopython
Description: gene prediction in eukaryotic genomes
 AUGUSTUS is a software for gene prediction in eukaryotic genomic sequences
 that is based on a generalized hidden Markov model (HMM), a probabilistic
 model of a sequence and its gene structure. After learning gene structures
 from a reference annotation, AUGUSTUS uses the HMM to recognize genes in a new
 sequence and annotates it with the regions of identified genes. External hints,
 e.g. from RNA sequencing, EST or protein alignments etc. can be used to guide
 and improve the gene finding process. The result is the set of most likely gene
 structures that comply with all given user constraints, if such gene
 structures exist.
 AUGUSTUS already includes prebuilt HMMs for many species, as well as scripts
 to train custom models using annotated genomes.

Package: augustus-data
Architecture: all
Depends: ${misc:Depends}
Description: data files for AUGUSTUS
 This package contains pre-trained species models and other required
 architecture-independent data files for the AUGUSTUS gene finder. These are
 needed to identify gene structures in these or related organisms.

Package: augustus-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: documentation files for AUGUSTUS
 This package contains documentation for AUGUSTUS: a comprehensive manual-style
 README both for AUGUSTUS in general as well as for its comparative gene
 prediction (cgp) mode, as well as a HTML tutorial.
